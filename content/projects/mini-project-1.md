+++
title = "Mini Project 1"
description = "Create a static site with Zola, a Rust static site generator, that will hold all of the portfolio work in this class.  Store source code in a GitLab repo in our Duke GitLab organization."
date = 2024-01-31


[taxonomies]
categories = ["Project"]
tags = ["Project", "Zola"]

[extra]
toc = true
comments = false
+++

## Requirements

Static site generated with Zola

Home page and portfolio project page templates

Styled with CSS

GitLab repo with source code

## Result

Link to live static site: [Site →](https://rivocx.gitlab.io/mini-project-1)

Link to GitLab repo: [Repo →](https://gitlab.com/RivocX/mini-project-1.git)

