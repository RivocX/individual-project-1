# Individual Project 1
> Puyang(Rivoc) Xu (px16)

This project is to build a Personal Website.

Access through Netlify: [Netlify Site →](https://rivoc-xu-web.netlify.app/)

Access through Gitlab: [Gitlab Site →](https://individual-project-1-rivocx-e88f429bf1afef4f13c1a225ad54b4db7e5.gitlab.io/)

Demo Video: [Video →](https://gitlab.com/RivocX/individual-project-1/-/blob/main/media/Demo%20Video.mov?ref_type=heads)

## Detailed steps
You need to install Zola for this project.

You can choose whatever theme you like. I used the [DeepThought](https://www.getzola.org/themes/deepthought/) theme on Zola official website.

Following the official guide, you should be able to access the website locally by running `zola serve`.

Modify md files as you like. Then you can get your personal website.


## GitLab Workflow
Add a `.gitlab-ci.yml` file with following codes.
```
stages:
  - deploy

default:
  image: debian:stable-slim
  tags:
    - docker

variables:
  # The runner will be able to pull your Zola theme when the strategy is
  # set to "recursive".
  GIT_SUBMODULE_STRATEGY: "recursive"

  # If you don't set a version here, your site will be built with the latest
  # version of Zola available in GitHub releases.
  # Use the semver (x.y.z) format to specify a version. For example: "0.17.2" or "0.18.0".
  ZOLA_VERSION:
    description: "The version of Zola used to build the site."
    value: ""

pages:
  stage: deploy
  script:
    - |
      apt-get update --assume-yes && apt-get install --assume-yes --no-install-recommends wget ca-certificates
      if [ $ZOLA_VERSION ]; then
        zola_url="https://github.com/getzola/zola/releases/download/v$ZOLA_VERSION/zola-v$ZOLA_VERSION-x86_64-unknown-linux-gnu.tar.gz"
        if ! wget --quiet --spider $zola_url; then
          echo "A Zola release with the specified version could not be found.";
          exit 1;
        fi
      else
        github_api_url="https://api.github.com/repos/getzola/zola/releases/latest"
        zola_url=$(
          wget --output-document - $github_api_url |
          grep "browser_download_url.*linux-gnu.tar.gz" |
          cut --delimiter : --fields 2,3 |
          tr --delete "\" "
        )
      fi
      wget $zola_url
      tar -xzf *.tar.gz
      ./zola build

  artifacts:
    paths:
      # This is the directory whose contents will be deployed to the GitLab Pages
      # server.
      # GitLab Pages expects a directory with this name by default.
      - public

  rules:
    # This rule makes it so that your website is published and updated only when
    # you push to the default branch of your repository (e.g. "master" or "main").
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```

Then git push it to GitLab. It will automatically build the Pipeline. It may take a few minutes. You can see a green check mark on GitLab once it's done.

You can see the link under `Deploy/Pages`:
![](./media/access_page.png)

## Hosting and Deployment
To host the website on Netlify, you need to create a `netlify.toml` file on your root folder. Then add following code.
```
[build]
# This assumes that the Zola site is in a docs folder. If it isn't, you don't need
# to have a `base` variable but you do need the `publish` and `command` variables.
publish = "/public"
command = "zola build"

[build.environment]
# Set the version name that you want to use and Netlify will automatically use it.
ZOLA_VERSION = "0.18.0"

[context.deploy-preview]
command = "zola build --base-url $DEPLOY_PRIME_URL"
```

Push it to Gitlab. Then sign up on Netlify using your GitLab account. Choose the corresponding project and deploy it. You can create your own domain name.

Mine is `rivoc-xu-web`.
![](./media/netlify.png)

Now you can access the website through the Netlify link.

## Website Functionality Screenshots
This personal website has many functions including Projects, Posts, Searching and so on. Here are some screenshots for its basic ones. For the detailed functions, please watch the demo video [Demo video →](https://gitlab.com/RivocX/individual-project-1/-/blob/main/media/Demo%20Video.mov?ref_type=heads)

### Home page
![](./media/Homepage.png)

### Projects
![](./media/projects.png)

### Project Detail
![](./media/project_detail.png)

### Posts
![](./media/posts.png)

### Tags
![](./media/tags.png)

### Categories
![](./media/categories.png)

### Search
![](./media/search.png)


