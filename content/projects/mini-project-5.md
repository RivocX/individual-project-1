+++
title = "Mini Project 5"
description = "Serverless Rust Microservice ."
date = 2024-03-02


[taxonomies]
categories = ["Project"]
tags = ["Project", "Rust", "AWS"]

[extra]
toc = true
comments = false
+++

## Requirements

Create a Rust AWS Lambda function (or app runner)

Implement a simple service

Connect to a database

# Week 5 Mini Project
> Puyang(Rivoc) Xu (px16)

This is a serverless rust microservice project. This microservice is a simple function that will return course id and professor's name based on given university and course name.

## Detailed steps
### Preparation
Install Cargo and AWS.

Create the project.
```
cargo lambda new Serverless_Rust_Microservice
```

Create a new role on AWS IAM. Make sure they have following permissions policies: 
* AmazonDynamoDBFullAccess
* AWSLambda_FullAccess
* AWSLambdaBasicExecutionRole

### Cargo project
In `Cargo.toml`, modify the dependencies as following.
```
lambda_runtime = "0.10.0"
serde = { version = "1.0", features = ["derive"] }
serde_json = "1.0"
aws-config = "0.51.0"
aws-sdk-dynamodb = "0.21.0"
rand = "0.8.5"
tokio = { version = "1", features = ["full"] }
```

In `/src/main.rs`, write a simple application. The application is to get the response of course id and professor's name based on given university and course name.

Run following codes to deploy this project on AWS lambda.
```
cargo lambda build --release
cargo lambda deploy --region us-west-2 --iam-role arn:aws:iam::339712859714:role/MiniProject5
```

### DynamoDB
Create a new table in AWS DynamoDB named ProfessorInfo. Use course_id as Partition key.
Click `Create item` to add some items.

### API gateway Trigger
Add an API gateway trigger to this lambda function. Create a REST API one.
Create a resource named `miniproject5-resource`. Add an ANY lambda function method to this resource and choose the lambda function. Then deploy this resource.

### Test
Create a new stage and use the given url for test. I use Postman for testing. You can also use `curl` command.

My url is as following.
```
https://xy0g7nvh7a.execute-api.us-west-2.amazonaws.com/test/Serverless_Rust_Microservice/miniproject5-resource
```
