+++
title = "Mini Project 3"
description = "Create an S3 Bucket using CDK with AWS CodeWhisperer."
date = 2024-02-16


[taxonomies]
categories = ["Project"]
tags = ["Project", "AWS"]

[extra]
toc = true
comments = false
+++

## Requirements

Create S3 bucket using AWS CDK

Use CodeWhisperer to generate CDK code

Add bucket properties like versioning and encryption

# Week 3 Mini Project

This is a project that create an S3 Bucket using CDK with AWS CodeWhisperer.

## Detailed steps
### Preparation
First you need to create a project in [CodeCatalyst](https://codecatalyst.aws/explore) and create a new empty dev environment(AWS Cloud9).

Then log in to AWS account, create a new user in IAM with corresponding permission policies and inline policies. Generate an access key for the new user and save its ID and the secret key as well. Use `aws configure` in Cloud9 IDE to set up the access key.

Create the project.
```
cdk init app --language=typescript
```
### Use of CodeWhisperer
Enable CodeWhisperer in Cloud9 IDE.

Then Use the following prompt to generate the S3 bucket code in the file `/lib/mini_proj3-stack.ts`

```
// make an S3 bucket and enable versioning and encryption
```  

Used the following prompt to generate the necessary variables in the file `/bin/mini_proj3.ts`

```
// add necessary variables to create the S3 bucket
```

### Deployment
Compile the TypeScript file and create the CloudFormation template.

```
npm run build

cdk synth
```

Deploy the template.

```
cdk bootstrap

cdk deploy
```