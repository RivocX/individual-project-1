+++
title = "Mini Project 4"
description = "Containerize a Rust Actix Web Service."
date = 2024-02-23


[taxonomies]
categories = ["Project"]
tags = ["Project", "Rust", "Docker"]

[extra]
toc = true
comments = false
+++

## Requirements

Containerize simple Rust Actix web app

Build Docker image

Run container locally

# Week 4 Mini Project
> Puyang(Rivoc) Xu (px16)

This is a project that containerize a Rust Actix Web Service using Docker. The web service is simple. You can either access a todo list or view the uptime on the root page.

## Detailed steps
### Preparation
First you need to install Cargo and Docker Desktop. Make sure Docker is running and working properly.

Create the project.
```
cargo new actix_web_service
cd actix_web_service 
```

### Cargo project
In `Cargo.toml`, add the following dependencies.
```
actix-web = "4.0"
chrono = "0.4.19" # Time
```

In `/src/main.rs`, write a simple application. Mine is to access a todo list or view the uptime on the root page. If you need more denpendencies, you may need to change your toml file.

Test your program locally by running following command.
```
cargo run
```

Then you can access the website by entering http://localhost:8080/ on your browser. You can also test it on your terminal by `curl` command.

### Containerize using Docker
Write a simple Dockerfile.

Run the following command. The Docker image is named `myapp` here.
```
sudo docker build -t myapp .

```

If there is some problems when pulling the image. You may need to pull this image manually to ensure it exists on Docker Hub. Suppose the image is `rust:1.75`, run the following command.
```
docker pull rust:1.75
```

Then run the Docker container.
```
sudo docker run -d -p 8080:8080 myapp

```

Now you can see the running container on your Docker Desktop. Click the external port link on the app to access the website. If you want to stop it, simply click stop on the Docker Desktop GUI or run the following command on a new terminal.
```
sudo docker stop [CONTAINER_ID_OR_NAME]
```
